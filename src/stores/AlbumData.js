import img1 from '../assets/albumPics/Migs _ Ressa-1.jpg';
import img2 from '../assets/albumPics/Migs _ Ressa-2.jpg';
import img21 from '../assets/albumPics/Migs _ Ressa-21.jpg';
import img30 from '../assets/albumPics/Migs _ Ressa-30.jpg';
import img33 from '../assets/albumPics/Migs _ Ressa-33.jpg';
import img35 from '../assets/albumPics/Migs _ Ressa-35.jpg';
import img36 from '../assets/albumPics/Migs _ Ressa-36.jpg';
import img37 from '../assets/albumPics/Migs _ Ressa-37.jpg';
import img39 from '../assets/albumPics/Migs _ Ressa-39.jpg';
import img40 from '../assets/albumPics/Migs _ Ressa-40.jpg';
import img43 from '../assets/albumPics/Migs _ Ressa-43.jpg';
import img48 from '../assets/albumPics/Migs _ Ressa-48.jpg';
import img50 from '../assets/albumPics/Migs _ Ressa-50.jpg';
import img51 from '../assets/albumPics/Migs _ Ressa-51.jpg';
import img52 from '../assets/albumPics/Migs _ Ressa-52.jpg';
import img59 from '../assets/albumPics/Migs _ Ressa-59.jpg';
import img61 from '../assets/albumPics/Migs _ Ressa-61.jpg';
import img62 from '../assets/albumPics/Migs _ Ressa-62.jpg';
import img63 from '../assets/albumPics/Migs _ Ressa-63.jpg';
import img67 from '../assets/albumPics/Migs _ Ressa-67.jpg';
import img70 from '../assets/albumPics/Migs _ Ressa-70.jpg';
import img71 from '../assets/albumPics/Migs _ Ressa-71.jpg';
import img73 from '../assets/albumPics/Migs _ Ressa-73.jpg';
import img75 from '../assets/albumPics/Migs _ Ressa-75.jpg';
import img77 from '../assets/albumPics/Migs _ Ressa-77.jpg';
import img80 from '../assets/albumPics/Migs _ Ressa-80.jpg';
import img81 from '../assets/albumPics/Migs _ Ressa-81.jpg';
import img82 from '../assets/albumPics/Migs _ Ressa-82.jpg';
import img83 from '../assets/albumPics/Migs _ Ressa-83.jpg';
import img84 from '../assets/albumPics/Migs _ Ressa-84.jpg';
import img85 from '../assets/albumPics/Migs _ Ressa-85.jpg';
import img89 from '../assets/albumPics/Migs _ Ressa-89.jpg';
import img93 from '../assets/albumPics/Migs _ Ressa-93.jpg';
import img94 from '../assets/albumPics/Migs _ Ressa-94.jpg';
import img95 from '../assets/albumPics/Migs _ Ressa-95.jpg';
import img99 from '../assets/albumPics/Migs _ Ressa-99.jpg';
import img103 from '../assets/albumPics/Migs _ Ressa-103.jpg';
import img105 from '../assets/albumPics/Migs _ Ressa-105.jpg';
import img107 from '../assets/albumPics/Migs _ Ressa-107.jpg';
import img108 from '../assets/albumPics/Migs _ Ressa-108.jpg';
import img109 from '../assets/albumPics/Migs _ Ressa-109.jpg';
import img111 from '../assets/albumPics/Migs _ Ressa-111.jpg';
import img112 from '../assets/albumPics/Migs _ Ressa-112.jpg';
import img113 from '../assets/albumPics/Migs _ Ressa-113.jpg';
import img114 from '../assets/albumPics/Migs _ Ressa-114.jpg';
import img117 from '../assets/albumPics/Migs _ Ressa-117.jpg';
import img118 from '../assets/albumPics/Migs _ Ressa-118.jpg';
import img120 from '../assets/albumPics/Migs _ Ressa-120.jpg';
import img121 from '../assets/albumPics/Migs _ Ressa-121.jpg';
import img122 from '../assets/albumPics/Migs _ Ressa-122.jpg';
import img131 from '../assets/albumPics/Migs _ Ressa-131.jpg';
import img136 from '../assets/albumPics/Migs _ Ressa-136.jpg';
import img152 from '../assets/albumPics/Migs _ Ressa-152.jpg';
import img158 from '../assets/albumPics/Migs _ Ressa-158.jpg';
import img159 from '../assets/albumPics/Migs _ Ressa-159.jpg';
import img160 from '../assets/albumPics/Migs _ Ressa-160.jpg';
import img162 from '../assets/albumPics/Migs _ Ressa-162.jpg';
import img163 from '../assets/albumPics/Migs _ Ressa-163.jpg';
import img167 from '../assets/albumPics/Migs _ Ressa-167.jpg';
import img172 from '../assets/albumPics/Migs _ Ressa-172.jpg';
import img176 from '../assets/albumPics/Migs _ Ressa-176.jpg';
import img177 from '../assets/albumPics/Migs _ Ressa-177.jpg';
import img180 from '../assets/albumPics/Migs _ Ressa-180.jpg';
import img204 from '../assets/albumPics/Migs _ Ressa-204.jpg';
import img206 from '../assets/albumPics/Migs _ Ressa-206.jpg';
import img244 from '../assets/albumPics/Migs _ Ressa-244.jpg';
import img255 from '../assets/albumPics/Migs _ Ressa-255.jpg';
import img256 from '../assets/albumPics/Migs _ Ressa-256.jpg';
import img257 from '../assets/albumPics/Migs _ Ressa-257.jpg';
import img259 from '../assets/albumPics/Migs _ Ressa-259.jpg';
import img260 from '../assets/albumPics/Migs _ Ressa-260.jpg';
import img262 from '../assets/albumPics/Migs _ Ressa-262.jpg';
import img264 from '../assets/albumPics/Migs _ Ressa-264.jpg';
import img265 from '../assets/albumPics/Migs _ Ressa-265.jpg';
import img266 from '../assets/albumPics/Migs _ Ressa-266.jpg';
import img267 from '../assets/albumPics/Migs _ Ressa-267.jpg';
import img275 from '../assets/albumPics/Migs _ Ressa-275.jpg';
import img280 from '../assets/albumPics/Migs _ Ressa-280.jpg';

let data = [
  {
    imgSrc: img1
  },
  {
    imgSrc: img39
  },
  {
    imgSrc: img21
  },
  {
    imgSrc: img30
  },
  {
    imgSrc: img33
  },
  {
    imgSrc: img35
  },
  {
    imgSrc: img36
  },
  {
    imgSrc: img37
  },
  {
    imgSrc: img2
  },
  {
    imgSrc: img40
  },
  {
    imgSrc: img43
  },
  {
    imgSrc: img48
  },
  {
    imgSrc: img50
  },
  {
    imgSrc: img51
  },
  {
    imgSrc: img52
  },
  {
    imgSrc: img59
  },
  {
    imgSrc: img61
  },
  {
    imgSrc: img62
  },
  {
    imgSrc: img63
  },
  {
    imgSrc: img67
  },
  {
    imgSrc: img70
  },
  {
    imgSrc: img71
  },
  {
    imgSrc: img73
  },
  {
    imgSrc: img75
  },
  {
    imgSrc: img77
  },
  {
    imgSrc: img80
  },
  {
    imgSrc: img81
  },
  {
    imgSrc: img82
  },
  {
    imgSrc: img83
  },
  {
    imgSrc: img84
  },
  {
    imgSrc: img85
  },
  {
    imgSrc: img89
  },
  {
    imgSrc: img93
  },
  {
    imgSrc: img94
  },
  {
    imgSrc: img95
  },
  {
    imgSrc: img99
  },
  {
    imgSrc: img103
  },
  {
    imgSrc: img105
  },
  {
    imgSrc: img107
  },
  {
    imgSrc: img108
  },
  {
    imgSrc: img109
  },
  {
    imgSrc: img111
  },
  {
    imgSrc: img112
  },
  {
    imgSrc: img113
  },
  {
    imgSrc: img114
  },
  {
    imgSrc: img117
  },
  {
    imgSrc: img118
  },
  {
    imgSrc: img120
  },
  {
    imgSrc: img121
  },
  {
    imgSrc: img122
  },
  {
    imgSrc: img131
  },
  {
    imgSrc: img136
  },
  {
    imgSrc: img152
  },
  {
    imgSrc: img158
  },
  {
    imgSrc: img159
  },
  {
    imgSrc: img160
  },
  {
    imgSrc: img162
  },
  {
    imgSrc: img163
  },
  {
    imgSrc: img167
  },
  {
    imgSrc: img172
  },
  {
    imgSrc: img176
  },
  {
    imgSrc: img177
  },
  {
    imgSrc: img180
  },
  {
    imgSrc: img204
  },
  {
    imgSrc: img206
  },
  {
    imgSrc: img244
  },
  {
    imgSrc: img255
  },
  {
    imgSrc: img256
  },
  {
    imgSrc: img257
  },
  {
    imgSrc: img259
  },
  {
    imgSrc: img260
  },
  {
    imgSrc: img262
  },
  {
    imgSrc: img264
  },
  {
    imgSrc: img265
  },
  {
    imgSrc: img266
  },
  {
    imgSrc: img267
  },
  {
    imgSrc: img275
  },
  {
    imgSrc: img280
  }
];

export default data;


