import './App.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { Container } from 'react-bootstrap';

// pages & components
import Home from './pages/Home';
import AppNavBar from './components/AppNavBar';
import Venue from './pages/Venue';
import Party from './pages/Party';
import Gift from './pages/Gift';
import Entourage from './pages/Entourage';
import Gallery from './pages/Gallery';
import Faqs from './pages/Faqs';
import Navs from './components/Navs'

function App() {
  return (
    <div>
      <Router>
        <AppNavBar/>
        <Container fluid style={{ padding: '0' }}>
          <Routes>
            <Route exact path ='/' element = {<Home/>}/>
            <Route exact path ='/venue' element = {<Venue/>}/>
            <Route exact path ='/party' element = {<Party/>}/>
            <Route exact path ='/giftGuide' element = {<Gift/>}/>
            <Route exact path ='/entourage' element = {<Entourage/>}/>
            <Route exact path ='/gallery' element = {<Gallery/>}/>
            <Route exact path ='/faqs' element = {<Faqs/>}/>
          </Routes>
        </Container>
        <Navs />
      </Router>
    </div>
  );
}

export default App;
