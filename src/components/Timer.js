import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';

const Timer = ({ targetDate }) => {
    const calculateTimeLeft = () => {
        const now = new Date();
        const difference = new Date(targetDate) - now;

        if (difference <= 0) {
            return {
                months: 0,
                days: 0,
                hours: 0,
                minutes: 0,
                seconds: 0,
            };
        }

        const months = Math.floor(difference / (1000 * 60 * 60 * 24 * 30.44)); // Average month length
        const days = Math.floor((difference % (1000 * 60 * 60 * 24 * 30.44)) / (1000 * 60 * 60 * 24));
        const hours = Math.floor((difference % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        const minutes = Math.floor((difference % (1000 * 60 * 60)) / (1000 * 60));
        const seconds = Math.floor((difference % (1000 * 60)) / 1000);

        return {
            months,
            days,
            hours,
            minutes,
            seconds,
        };
    };

    const [timeLeft, setTimeLeft] = useState(calculateTimeLeft());

    useEffect(() => {
        const timer = setInterval(() => {
            setTimeLeft(calculateTimeLeft());
        }, 1000);

        return () => {
            clearInterval(timer);
        };
    }, []);

    return (
    	<Container className='timerContent'>
    		<Row className="countdown-timer d-flex justify-content-center align-items-center">
	            <h1 className='dancingScript text-white'>Countdown to forever:</h1>
                <Col xs={2} md={1} className="text-center rounded text-white mx-md-3 mx-lg-2" >
	                <h2 className="mb-0">{timeLeft.months}</h2>
	                <p className="mb-0 greatVibes">mos</p>
	            </Col>
	            <Col xs={2} md={1} className="text-center rounded text-white mx-md-3 mx-lg-2" >
	                <h2 className="mb-0">{timeLeft.days}</h2>
	                <p className="mb-0 greatVibes">days</p>
	            </Col>
	            <Col xs={2} md={1} className="text-center rounded text-white mx-md-3 mx-lg-2" >
	                <h2 className="mb-0">{timeLeft.hours}</h2>
	                <p className="mb-0 greatVibes">hrs</p>
	            </Col>
	            <Col xs={2} md={1} className="text-center rounded text-white mx-md-3 mx-lg-2" >
	                <h2 className="mb-0">{timeLeft.minutes}</h2>
	                <p className="mb-0 greatVibes">mins</p>
	            </Col>
	            <Col xs={2} md={1} className="text-center rounded text-white mx-md-3 mx-lg-2" >
	                <h2 className="mb-0">{timeLeft.seconds}</h2>
	                <p className="mb-0 greatVibes">secs</p>
	            </Col>
	        </Row>
    	</Container>
        
    );
};

export default Timer;
